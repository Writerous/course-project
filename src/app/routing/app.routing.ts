import {Routes} from '@angular/router';
import {IntroComponent} from '../states/intro/intro.component';
import {LoginComponent} from '../states/login/login.component';
import {NotFoundComponent} from '../states/not-found/not-found.component';

export const appRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: '',
        component: IntroComponent,
    },
    {path: '**', component: NotFoundComponent},
];
