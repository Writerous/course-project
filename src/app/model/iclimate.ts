export interface IClimate {
    minTemp?: string;
    maxTemp?: string;
    humidity?: number;
    pressure?: number;
    icon: string;
    description: string;
    city: string;
    ready: boolean;
}
