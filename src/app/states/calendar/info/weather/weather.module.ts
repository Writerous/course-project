import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WeatherComponent} from './weather.component';
import {HttpClientModule} from '@angular/common/http';
import {WeatherHttpService} from './services/weather-http.service';

@NgModule({
    declarations: [WeatherComponent],
    exports: [WeatherComponent],
    imports: [CommonModule, HttpClientModule],
    providers: [WeatherHttpService],
})
export class WeatherModule {}
