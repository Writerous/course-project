import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {IClimate} from 'src/app/model/iclimate';
import {catchError} from 'rxjs/internal/operators';

@Injectable()
export class WeatherHttpService {
    constructor(private http: HttpClient) {}

    getWeather(url: string): Observable<any> {
        return this.http.get(url).pipe(catchError(() => of(this.servError)));
    }
    climateDefault = {
        description: 'coming soon',
        city: 'Your city',
        icon: `assets/images/soon.png`,
        ready: false,
    };

    climateError = {
        description: 'dont`t worry',
        city: 'not found',
        icon: `assets/images/error.png`,
        ready: false,
    };

    servError = {
        data: {
            weather: [undefined],
            request: [
                {
                    query: null,
                },
            ],
        },
    };

    shortDescs = [
        'thunder',
        'rain shower',
        'ice pellets',
        'freezing rain',
        'partly cloudy',
        'cloudy',
        'overcast',
        'mist',
        'rain',
        'snow',
        'sleet',
        'drizzle',
        'thundery',
        'blizzard',
        'fog',
        'clear',
        'sunny',
    ];

    climateChoice(today: Date, date: Date, serv: any, servPast: any): IClimate {
        const dateStamp = date.getTime();
        const todayStamp = today.getTime();
        const dayStamp = 86400000;

        if (dateStamp - todayStamp > dayStamp * 10) {
            return this.climateDefault;
        }

        if (dateStamp >= todayStamp) {
            return this.toFind(date, serv.data);
        }

        if (todayStamp - dateStamp <= dayStamp * 10) {
            return this.toFind(date, servPast.data);
        }

        return this.climateDefault;
    }

    private toFind(dateSel: Date, data: any): IClimate {
        if (!data.weather[0]) {
            return this.climateError;
        }

        return this.climate(
            data,
            data.weather.find(
                (item: any) => dateSel.toISOString().slice(0, 10) === item.date,
            ),
        );
    }

    climate(data: any, day: any): IClimate {
        if (!day) {
            return this.climateError;
        }

        const description: string = day.hourly[0].weatherDesc[0].value.toLocaleLowerCase();

        const shortDesc = this.shortDescs.find(item => description.includes(item));

        const icon =
            `http://openweathermap.org/img/wn/${this.iconSel(shortDesc)}@2x.png` ||
            day.hourly[0].weatherIconUrl[0].value;

        return {
            minTemp: day.mintempC,
            maxTemp: day.maxtempC,
            humidity: day.hourly[0].humidity,
            pressure: Math.round(day.hourly[0].pressure / 1.332),
            icon: icon,
            description: shortDesc[0].toLocaleUpperCase() + shortDesc.slice(1),
            city: data.request[0].query.split(',')[0],
            ready: true,
        };
    }

    private iconSel(desk: string): string {
        switch (desk) {
            case 'thunder':
                return '11d';
            case 'thundery':
                return '11d';
            case 'partly cloudy':
                return '02d';
            case 'cloudy':
                return '03d';
            case 'overcast':
                return '04d';
            case 'rain shower':
                return '09d';
            case 'drizzle':
                return '09d';
            case 'rain':
                return '10d';
            case 'snow':
                return '13d';
            case 'mist':
                return '50d';
            case 'fog':
                return '50d';
            case 'freezing rain':
                return '13d';
            case 'sleet':
                return '13d';
            case 'clear':
                return '01d';
            case 'ice pellets':
                return '13d';
            case 'blizzard':
                return '13d';
            case 'sunny':
                return '01d';
            default:
                return '13d';
        }
    }

    indicate(climate: IClimate): Array<any> {
        return [
            {
                icon: 'assets/images/temp6.png',
                info: `${climate && climate.minTemp} \xB0C ... ${climate &&
                    climate.maxTemp} \xB0C`,
                class: 'weather-temp-logo',
            },
            {
                icon: 'assets/images/hum3.png',
                info: `${climate && climate.humidity} %`,
                class: 'weather-hum-logo',
            },
            {
                icon: 'assets/images/bar2.png',
                info: `${climate && climate.pressure} mmHg`,
                class: 'weather-bar-logo',
            },
        ];
    }
}
