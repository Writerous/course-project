import {Component, OnInit, Input} from '@angular/core';
import {WeatherHttpService} from './services/weather-http.service';
import {BehaviorSubject, combineLatest, of} from 'rxjs';
import {IClimate} from '../../../../model/iclimate';
import {switchMap, filter, tap} from 'rxjs/internal/operators';

const apiKey = '8c04f673a49e4968ae1203023201104';
const forecastURL =
    `https://api.worldweatheronline.com/premium/v1/weather.ashx?q=Moscow&num_of_days=11&tp=24&format=json&key=${apiKey}`;

@Component({
    selector: 'pro-weather',
    templateUrl: './weather.component.html',
    styleUrls: ['./weather.component.css'],
})
export class WeatherComponent implements OnInit {
    @Input() set date(value: Date) {
        if (value !== this.date) {
            this.dateSubject.next(value);
        }
    }

    get date(): Date {
        return this.dateSubject.getValue();
    }

    constructor(private weatherHttpService: WeatherHttpService) {}

    today: Date;
    climate: IClimate;
    dayStamp = 86400000;
    todayStamp: number;
    loadingFinish: boolean;
    private dateSubject = new BehaviorSubject<Date>(this.today);

    indications: Array<any>;

    ngOnInit() {
        const now = new Date();

        this.today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12);
        this.todayStamp = this.today.getTime();
        this.loadingFinish = false;
        combineLatest(
            this.dateSubject,
            this.weatherHttpService.getWeather(forecastURL),
            this.weatherHttpService.getWeather(this.pastURL()),
        ).subscribe(value => {
            this.climate = this.weatherHttpService.climateChoice(this.today, ...value);
            this.indications = this.weatherHttpService.indicate(this.climate);
            this.loadingFinish = true;
        });
        this.onceFromPast();
    }

    private pastURL(): string {
        const fromDate = new Date(this.todayStamp - this.dayStamp * 10);
        const toDate = new Date(this.todayStamp - this.dayStamp);

        return `https://api.worldweatheronline.com/premium/v1/past-weather.ashx?q=Moscow&date=${fromDate
            .toISOString()
            .slice(0, 10)}&enddate=${toDate
            .toISOString()
            .slice(0, 10)}&tp=24&format=json&key=${apiKey}`;
    }

    private onceFromPast() {
        this.dateSubject
            .pipe(
                filter(
                    date =>
                        this.todayStamp - date.getTime() > this.dayStamp * 10 &&
                        date.getFullYear() > 2008,
                ),
                tap(() => (this.loadingFinish = false)),
                switchMap(date =>
                    this.weatherHttpService.getWeather(this.oncePastURL(date)),
                ),
            )
            .subscribe(serv => {
                this.climate = this.weatherHttpService.climate(
                    serv.data,
                    serv.data.weather[0],
                );
                this.indications = this.weatherHttpService.indicate(this.climate);
                this.loadingFinish = true;
            });
    }

    private oncePastURL(date: Date): string {
        return `https://api.worldweatheronline.com/premium/v1/past-weather.ashx?q=Moscow&date=${date
            .toISOString()
            .slice(0, 10)}&tp=24&format=json&key=${apiKey}`;
    }
}
