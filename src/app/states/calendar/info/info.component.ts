import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {map} from 'rxjs/internal/operators';

@Component({
    selector: 'pro-info',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.css'],
})
export class InfoComponent implements OnInit {
    constructor(private route: ActivatedRoute) {}

    date: Date;

    ngOnInit() {
        const now = new Date();
        const today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12);

        this.route.paramMap
            .pipe(
                map(params => {
                    if (!params.get('id').match(/^20[0-2][0-9]-\d{2}-\d{2}$/g)) {
                        return today.toISOString().slice(0, 10);
                    }

                    return params.get('id');
                }),
            )
            .subscribe((dateId: string) => {
                const dates = dateId
                    .match(/^\d{4}-\d{2}-\d{2}$/g)[0]
                    .split('-')
                    .map(x => Number.parseInt(x, 10));

                this.date = new Date(dates[0], dates[1] - 1, dates[2], 12);
            });
    }
}
