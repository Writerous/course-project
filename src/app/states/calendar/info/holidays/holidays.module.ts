import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HolidaysComponent} from './holidays.component';
import {HttpClientModule} from '@angular/common/http';
import {HolidaysHttpService} from './services/holidays-http.service';

@NgModule({
    declarations: [HolidaysComponent],
    exports: [HolidaysComponent],
    imports: [CommonModule, HttpClientModule],
    providers: [HolidaysHttpService],
})
export class HolidaysModule {}
