import {Component, OnInit, Input} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {HolidaysHttpService} from './services/holidays-http.service';
import {switchMap} from 'rxjs/internal/operators';

@Component({
    selector: 'pro-holidays',
    templateUrl: './holidays.component.html',
    styleUrls: ['./holidays.component.css'],
})
export class HolidaysComponent implements OnInit {
    @Input() set date(value: Date) {
        if (value !== this.date) {
            this.dateSubject.next(value);
        }
    }

    get date(): Date {
        return this.dateSubject.getValue();
    }

    today: Date;
    holidays = {
        rusHolidays: {
            name: '',
            ready: false,
        },
        interHolidays: {
            name: '',
            ready: false,
        },
    };
    dayOff: string;

    constructor(private holidaysHttpService: HolidaysHttpService) {}

    private dateSubject = new BehaviorSubject<Date>(this.today);

    ngOnInit() {
        const now = new Date();

        this.today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12);

        this.holiPerDate('rusHolidays');
        this.holiPerDate('interHolidays');
        this.dayOffPerDate();
    }

    private holiPerDate(typeHoli: string) {
        this.dateSubject
            .pipe(
                switchMap(date =>
                    this.holidaysHttpService.getPerDate(`${typeHoli}`, date),
                ),
            )
            .subscribe(holi => (this.holidays[typeHoli] = holi));
    }

    private dayOffPerDate() {
        this.dateSubject
            .pipe(switchMap(date => this.holidaysHttpService.getDayOff(date)))
            .subscribe(day => (this.dayOff = day));
    }
}
