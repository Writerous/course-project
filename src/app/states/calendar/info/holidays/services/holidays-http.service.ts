import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map, catchError} from 'rxjs/internal/operators';
import {IHoli} from 'src/app/model/iholi';

const BASE_URL = `${environment.firebaseConfig.databaseURL}`;
const DAYOF_URL = 'https://isdayoff.ru';

@Injectable()
export class HolidaysHttpService {
    constructor(private http: HttpClient) {}

    getPerDate(typeHoli: string, date: Date): Observable<IHoli> {
        const urlDate = `${date.getMonth()} ${date.getDate()}`;

        return this.http.get(`${BASE_URL}/${typeHoli}/${urlDate}.json`).pipe(
            map(
                data => Object.values(data).map(value => ({name: value, ready: true}))[0],
            ),
            catchError(() => of({name: '', ready: false})),
        );
    }

    getDayOff(date: Date): Observable<string> {
        const year = date.getFullYear();
        const weekday = date.getDay();

        if (year < 2004 || year > 2025) {
            const day = weekday === 0 || weekday === 6 ? 1 : 0;

            return of(this.toDayOff(day));
        }

        const dateURL = date
            .toISOString()
            .slice(0, 10)
            .split('-')
            .join('');

        return this.http.get(`${DAYOF_URL}/${dateURL}?pre=1`).pipe(
            map(data => this.toDayOff(data)),
            catchError(err => of(this.toDayOff(err))),
        );
    }

    private toDayOff(day: any): string {
        switch (day) {
            case 0:
                return 'Working Day (';
            case 1:
                return 'Day Off!';
            case 2:
                return 'Pre-holiday Day )';
            case 100:
                return 'error in date';
            case 101:
                return 'data is not found';
            default:
                return 'oops..';
        }
    }
}
