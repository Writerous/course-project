import {TestBed} from '@angular/core/testing';

import {HolidaysHttpService} from './holidays-http.service';

describe('HolidaysHttpService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: HolidaysHttpService = TestBed.get(HolidaysHttpService);
        expect(service).toBeTruthy();
    });
});
