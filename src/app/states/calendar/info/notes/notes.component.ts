import {Component, OnInit, Input} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BehaviorSubject, Subject} from 'rxjs';
import {INotes} from 'src/app/model/inotes';
import {switchMap} from 'rxjs/internal/operators';
import {NotesHttpService} from './services/notes-http.service';

@Component({
    selector: 'pro-notes',
    templateUrl: './notes.component.html',
    styleUrls: ['./notes.component.css'],
})
export class NotesComponent implements OnInit {
    @Input() set date(value: Date) {
        if (value !== this.date) {
            this.dateSubject.next(value);
        }
    }

    get date(): Date {
        return this.dateSubject.getValue();
    }
    constructor(
        private formBuilder: FormBuilder,
        private notesHttpService: NotesHttpService,
    ) {}

    notes: INotes;
    form: FormGroup;
    today: Date;
    pristine = true;
    private dateSubject = new BehaviorSubject<Date>(this.today);
    private noteSubject = new Subject<INotes>();

    ngOnInit() {
        const now = new Date();

        this.today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12);

        this.getNotes();
        this.addNotes();
        this.forms();
    }

    forms() {
        this.form = this.formBuilder.group({
            notes: [''],
        });

        this.form.valueChanges.subscribe(value => {
            this.pristine = false;
        });
    }

    onSubmit() {
        const notes: INotes = {
            content: this.form.value.notes,
        };

        this.noteSubject.next(notes);
        this.pristine = true;
    }

    addNotes() {
        this.dateSubject
            .pipe(
                switchMap(date =>
                    this.noteSubject.pipe(
                        switchMap(note => this.notesHttpService.addNotes(date, note)),
                    ),
                ),
            )
            // tslint:disable-next-line:no-console
            .subscribe(content => console.log(content));
    }

    getNotes() {
        this.dateSubject
            .pipe(switchMap(date => this.notesHttpService.getNotes(date)))
            .subscribe(notes => {
                this.notes = notes;
                this.form.setValue({notes: this.notes.content});
                this.pristine = true;
            });
    }
}
