import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotesComponent} from './notes.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NotesHttpService} from './services/notes-http.service';

@NgModule({
    declarations: [NotesComponent],
    exports: [NotesComponent],
    imports: [CommonModule, ReactiveFormsModule, HttpClientModule],
    providers: [NotesHttpService],
})
export class NotesModule {}
