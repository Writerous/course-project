import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {catchError, map, filter, take, switchMap} from 'rxjs/internal/operators';
import {of, Observable} from 'rxjs';
import {INotes} from 'src/app/model/inotes';
import {AuthService} from 'src/app/states/login/auth/auth.service';

const BASE_URL = `${environment.firebaseConfig.databaseURL}`;

@Injectable()
export class NotesHttpService {
    constructor(private http: HttpClient, private authService: AuthService) {}

    getNotes(date: Date): Observable<INotes> {
        const urlDate = date.toISOString().slice(0, 10);

        return this.authService.user$.pipe(
            filter(user => !!user),
            take(1),
            switchMap(({uid}) =>
                this.http.get(`${BASE_URL}/users/${uid}/notes/${urlDate}.json`).pipe(
                    map(
                        data =>
                            Object.values(data).map(value => ({
                                content: value,
                                date: urlDate,
                            }))[0],
                    ),
                    catchError(() => of({date: urlDate, content: ''})),
                ),
            ),
        );
    }

    addNotes(date: Date, notes: INotes): Observable<Object> {
        const urlDate = date.toISOString().slice(0, 10);

        return this.authService.user$.pipe(
            filter(user => !!user),
            take(1),
            switchMap(({uid}) =>
                this.http.put(`${BASE_URL}/users/${uid}/notes/${urlDate}.json`, notes),
            ),
        );
    }
}
