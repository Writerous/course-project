import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InfoComponent} from './info.component';
import {WeatherModule} from './weather/weather.module';
import {HolidaysModule} from './holidays/holidays.module';
import {NotesModule} from './notes/notes.module';

@NgModule({
    declarations: [InfoComponent],
    exports: [InfoComponent],
    imports: [CommonModule, WeatherModule, HolidaysModule, NotesModule],
})
export class InfoModule {}
