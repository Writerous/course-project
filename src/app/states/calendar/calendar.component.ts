import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'pro-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.css'],
})
export class CalendarComponent implements OnInit {
    constructor(private router: Router) {}

    ngOnInit() {
        if (this.router.url === '/calend') {
            const now = new Date();
            const today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12)
                .toISOString()
                .slice(0, 10);

            this.router.navigate([`calend/${today}`]);
        }
    }
}
