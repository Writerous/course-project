import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CalendarComponent} from './calendar.component';
import {CalendNaviModule} from './calend-navi/calend-navi.module';
import {InfoModule} from './info/info.module';
import {RouterModule} from '@angular/router';
import {infoRoutes} from './routing/info.routing';

@NgModule({
    declarations: [CalendarComponent],
    exports: [CalendarComponent],
    imports: [
        CommonModule,
        CalendNaviModule,
        InfoModule,
        RouterModule.forChild(infoRoutes),
    ],
})
export class CalendarModule {}
