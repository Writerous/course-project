import {Component, OnInit} from '@angular/core';
import {IDay} from '../../../model/iday';
import {DaysMapService} from './services/days-map.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
    selector: 'pro-calend-navi',
    templateUrl: './calend-navi.component.html',
    styleUrls: ['./calend-navi.component.css'],
})
export class CalendNaviComponent implements OnInit {
    months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ];
    date: Date;
    today: Date;
    weeksMap: IDay[][];
    dateSelected: Date;

    constructor(
        private daysMap: DaysMapService,
        private router: Router,
        private location: Location,
    ) {}

    ngOnInit() {
        const now = new Date();

        this.date = new Date();
        this.today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12);
        this.weeksMap = this.daysMap.weeks(this.today);

        const url = this.router.url;

        if (url.match(/^\/calend\/20[0-2][0-9]-\d{2}-\d{2}$/g)) {
            const dateURL: string = url.match(/\d{4}-\d{2}-\d{2}/g)[0];
            const dates: number[] = dateURL.split('-').map(x => Number.parseInt(x, 10));

            this.dateSelected = new Date(dates[0], dates[1] - 1, dates[2], 12);
            this.date = this.dateSelected;
        } else if (url.match(/\/calend\//g)) {
            this.date = this.today;
        }

        this.weeksMap = this.daysMap.weeks(this.date);
        this.location.go(`calend/${this.date.toISOString().slice(0, 10)}`);
    }

    decMonth() {
        this.date = new Date(this.date.getFullYear(), this.date.getMonth() - 1);
        this.weeksMap = this.daysMap.weeks(this.date);
    }

    incMonth() {
        this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 1);
        this.weeksMap = this.daysMap.weeks(this.date);
    }

    get year() {
        return this.date.getFullYear();
    }

    get month() {
        return this.date.getMonth();
    }

    get veryBig() {
        return this.date.getFullYear() === 2029 && this.date.getMonth() === 11;
    }

    get veryLittle() {
        return this.date.getFullYear() === 2000 && this.date.getMonth() === 0;
    }
}
