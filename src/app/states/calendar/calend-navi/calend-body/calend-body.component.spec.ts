import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CalendBodyComponent} from './calend-body.component';

describe('CalendBodyComponent', () => {
    let component: CalendBodyComponent;
    let fixture: ComponentFixture<CalendBodyComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CalendBodyComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CalendBodyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
