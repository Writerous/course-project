import {Component, OnInit, Input} from '@angular/core';
import {IDay} from '../../../../model/iday';

@Component({
    selector: 'pro-calend-body',
    templateUrl: './calend-body.component.html',
    styleUrls: ['./calend-body.component.css'],
})
export class CalendBodyComponent implements OnInit {
    @Input() year: number;
    @Input() month: number;
    @Input() weeksMap: IDay[][];
    @Input() today: Date;
    @Input() dateSelected?: Date;
    weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

    constructor() {}

    ngOnInit() {}

    get weeks() {
        return this.weeksMap;
    }

    dateTransport(dateSel: Date) {
        this.dateSelected = dateSel;
    }
}
