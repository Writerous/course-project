import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CalendBodyComponent} from './calend-body.component';
import {DayComponent} from './day/day.component';
import {RouterModule} from '@angular/router';

@NgModule({
    declarations: [CalendBodyComponent, DayComponent],
    exports: [CalendBodyComponent],
    imports: [CommonModule, RouterModule.forChild([])],
})
export class CalendBodyModule {}
