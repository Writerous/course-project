import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {IDay} from 'src/app/model/iday';
import {Router} from '@angular/router';

@Component({
    selector: 'pro-day',
    templateUrl: './day.component.html',
    styleUrls: ['./day.component.css'],
})
export class DayComponent implements OnInit {
    @Input() day: IDay;
    @Input() month: number;
    @Input() year: number;
    @Input() today: Date;
    @Input() dateSelected?: Date;
    @Output() onDaySelected = new EventEmitter<Date>();
    isToday: boolean;
    isSelect: boolean;

    constructor(private router: Router) {}

    ngOnInit() {}

    ngOnChanges(): void {
        this.isToday = this.thisDay(this.today);
        this.isSelect = this.thisDay(this.dateSelected);
    }

    onSelect() {
        if (this.day.fromMonth === 1) {
            const date = new Date(this.year, this.month, this.day.day, 12);

            this.onDaySelected.emit(date);
            this.router.navigate([`calend/${date.toISOString().slice(0, 10)}`]);
        }
    }

    thisDay(daySel?: Date): boolean {
        if (!daySel) {
            return false;
        }

        const currDate = new Date(
            this.year,
            this.month + this.day.fromMonth - 1,
            this.day.day,
        );

        if (daySel.getDate() !== currDate.getDate()) {
            return false;
        }

        return (
            daySel.getMonth() === currDate.getMonth() &&
            daySel.getFullYear() === currDate.getFullYear()
        );
    }

    get isActive() {
        return this.day.fromMonth === 1;
    }
}
