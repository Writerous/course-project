import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CalendNaviComponent} from './calend-navi.component';
import {CalendBodyModule} from './calend-body/calend-body.module';
import {DaysMapService} from './services/days-map.service';

@NgModule({
    declarations: [CalendNaviComponent],
    exports: [CalendNaviComponent],
    imports: [CommonModule, CalendBodyModule],
    providers: [DaysMapService],
})
export class CalendNaviModule {}
