import {Injectable} from '@angular/core';
import {IDay} from 'src/app/model/iday';

@Injectable()
export class DaysMapService {
    constructor() {}

    private calendarArray(year: number, month: number): IDay[] {
        const date = new Date(year, month);
        const weekDayFirst = date.getDay() === 0 ? 6 : date.getDay() - 1;
        const currMonthLastDay = new Date(year, month + 1, 0);
        const lastDay = currMonthLastDay.getDate();
        const prevMonthLastDay = new Date(year, month, 0);
        const lastDayOfPrevMonth = prevMonthLastDay.getDate();

        const prevMonth = [];
        const nextMonth = [];
        const currMonth = [];

        for (let i = 0; i < weekDayFirst; i++) {
            const day: IDay = {day: lastDayOfPrevMonth - i, fromMonth: 0};

            prevMonth.unshift(day);
        }

        for (let i = 0; i < lastDay; i++) {
            const day: IDay = {day: i + 1, fromMonth: 1};

            currMonth.push(day);
        }

        const lastPosition = weekDayFirst + lastDay;
        const nextMonthCalc = lastPosition > 35 ? 42 - lastPosition : 35 - lastPosition;

        for (let i = 0; i < nextMonthCalc; i++) {
            const day: IDay = {day: i + 1, fromMonth: 2};

            nextMonth.push(day);
        }

        return prevMonth.concat(currMonth, nextMonth);
    }

    weeks(date: Date): IDay[][] {
        const days = this.calendarArray(date.getFullYear(), date.getMonth());
        const weeks = [];

        for (let i = 0; i < Math.floor(days.length / 7); i++) {
            weeks.push(days.slice(i * 7, (i + 1) * 7));
        }

        return weeks;
    }
}
