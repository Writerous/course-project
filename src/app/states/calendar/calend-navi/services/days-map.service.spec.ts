import {TestBed} from '@angular/core/testing';

import {DaysMapService} from './days-map.service';

describe('DaysMapService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: DaysMapService = TestBed.get(DaysMapService);

        expect(service).toBeTruthy();
    });
});
