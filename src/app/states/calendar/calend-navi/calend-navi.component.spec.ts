import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CalendNaviComponent} from './calend-navi.component';

describe('CalendNaviComponent', () => {
    let component: CalendNaviComponent;
    let fixture: ComponentFixture<CalendNaviComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CalendNaviComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CalendNaviComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
