import {CalendarComponent} from '../calendar.component';
import {Routes} from '@angular/router';
import {InfoComponent} from 'src/app/states/calendar/info/info.component';
import {AuthGuard} from 'src/app/states/login/auth/auth.guard';

export const infoRoutes: Routes = [
    {
        path: 'calend',
        canActivate: [AuthGuard],
        component: CalendarComponent,
        children: [
            {
                path: ':id',
                component: InfoComponent,
            },
        ],
    },
];
