import {Component, OnInit} from '@angular/core';
import {AuthService} from './auth/auth.service';
import {FormGroup, FormBuilder, AbstractControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';

const USER_NOT_FOUND_ERROR = 'auth/user-not-found';
const WRONG_PASSWORD_ERROR = 'auth/wrong-password';
const EMAILE_EXISTS_ERROR = 'auth/email-already-in-use';
const INVALID_EMAIL_ERROR = 'auth/invalid-email';
const DEFAULT = 'DEFAULT';
const ERROR_MESSAGE = {
    [DEFAULT]: 'Unknown error. Please try later',
    [USER_NOT_FOUND_ERROR]: 'Wrong email and(or) password',
    [WRONG_PASSWORD_ERROR]: 'Wrong email and(or) password',
    [EMAILE_EXISTS_ERROR]: 'The email address is already in use by another account.',
    [INVALID_EMAIL_ERROR]: 'The email address is badly formatted',
};

@Component({
    selector: 'pro-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
    form: FormGroup;
    errorMessage = '';

    constructor(
        private authService: AuthService,
        private formBuilder: FormBuilder,
        private router: Router,
    ) {}

    get email(): AbstractControl {
        return this.form.get('email');
    }

    get password(): AbstractControl {
        return this.form.get('password');
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            email: [
                '',
                [
                    Validators.required,
                    Validators.email,
                    Validators.pattern(/[.\w-]+@([\w-]+\.)+[\w-]+/),
                ],
            ],
            password: ['', [Validators.required, Validators.minLength(6)]],
        });
    }

    submit() {
        if (this.form.invalid) {
            return;
        }

        this.errorMessage = '';

        const {email, password} = this.form.value;

        this.authService.login(email, password).subscribe(
            () => {
                this.form.reset();
                this.router.navigate(['calend']);
            },
            error => {
                const code = error && error.code ? error.code : null;

                this.errorMessage =
                    ERROR_MESSAGE[code] || error.message || ERROR_MESSAGE.DEFAULT;
            },
        );
    }

    signUp() {
        if (this.form.invalid) {
            return;
        }

        const {email, password} = this.form.value;

        this.authService.signup(email, password).subscribe(
            () => {
                this.form.reset();
                this.router.navigate(['calend']);
            },
            error => {
                const code = error && error.code ? error.code : null;

                this.errorMessage =
                    ERROR_MESSAGE[code] || error.message || ERROR_MESSAGE.DEFAULT;
            },
        );
    }
}
