import {Injectable} from '@angular/core';
import {BehaviorSubject, from, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {IUser} from '../../../model/iuser';
import {AngularFireAuth} from '@angular/fire/auth';
import {map} from 'rxjs/internal/operators';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    private _user$ = new BehaviorSubject<IUser>(null);

    redirectUrl = '';

    constructor(private afAuth: AngularFireAuth, private router: Router) {
        this.afAuth.authState
            .pipe(
                map(fullUser => {
                    if (fullUser) {
                        return {uid: fullUser.uid, email: fullUser.email};
                    }

                    return null;
                }),
            )
            .subscribe(user => {
                this._user$.next(user);

                if (this.redirectUrl) {
                    this.router.navigate([this.redirectUrl]);
                    this.redirectUrl = '';
                }
            });
    }

    get user$(): Observable<IUser> {
        return this._user$.asObservable();
    }

    get isLoggedIn$(): Observable<boolean> {
        return this._user$.asObservable().pipe(map(user => !!user));
    }

    login(email: string, password: string): Observable<any> {
        return from(this.afAuth.auth.signInWithEmailAndPassword(email, password));
    }

    signup(email: string, password: string): Observable<any> {
        return from(this.afAuth.auth.createUserWithEmailAndPassword(email, password));
    }

    logout() {
        this.afAuth.auth.signOut();
        this.router.navigate(['']);
    }

    changeUser() {
        this.afAuth.auth.signOut();
        this.router.navigate(['/login']);
    }
}
