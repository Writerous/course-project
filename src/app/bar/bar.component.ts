import {Component, OnInit} from '@angular/core';
import {AuthService} from '../states/login/auth/auth.service';
import {Observable} from 'rxjs';
import {IUser} from '../model/iuser';
import {Router} from '@angular/router';

@Component({
    selector: 'pro-bar',
    templateUrl: './bar.component.html',
    styleUrls: ['./bar.component.css'],
})
export class BarComponent implements OnInit {
    user$: Observable<IUser>;
    isLoggedIn$: Observable<boolean>;
    today: string;

    constructor(private authService: AuthService, private router: Router) {}

    ngOnInit() {
        this.user$ = this.authService.user$;
        this.isLoggedIn$ = this.authService.isLoggedIn$;

        const now = new Date();

        this.today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12)
            .toISOString()
            .slice(0, 10);
    }

    logOut() {
        this.authService.logout();
    }

    changeUser() {
        this.authService.changeUser();
    }

    get url() {
        return this.router.url;
    }

    get dateURL() {
        return this.router.url.match(/^\/calend\/\d{4}-\d{2}-\d{2}$/g);
    }

    /*     get isToday() {
        return this.router.url === `/calend/${this.today}`;
    } */
}
