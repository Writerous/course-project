import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BarComponent} from './bar.component';
import {RouterModule} from '@angular/router';

@NgModule({
    declarations: [BarComponent],
    exports: [BarComponent],
    imports: [CommonModule, RouterModule.forChild([])],
})
export class BarModule {}
