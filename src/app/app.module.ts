import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {CalendarModule} from './states/calendar/calendar.module';
import {AppComponent} from './app.component';
import {registerLocaleData} from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import {IntroModule} from './states/intro/intro.module';
import {RouterModule} from '@angular/router';
import {appRoutes} from './routing/app.routing';
import {LoginModule} from './states/login/login.module';
import {BarModule} from './bar/bar.module';
import {environment} from 'src/environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {NotFoundModule} from './states/not-found/not-found.module';

registerLocaleData(localeRu, 'ru');

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        CalendarModule,
        IntroModule,
        RouterModule.forRoot(appRoutes),
        LoginModule,
        BarModule,
        NotFoundModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireAuthModule,
    ],
    providers: [
        {
            provide: LOCALE_ID,
            useValue: 'ru',
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
